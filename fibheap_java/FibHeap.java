import java.util.Iterator;
import java.util.ArrayList;

public class FibHeap<T extends FibHeap.Nodeable<T>> {

    // The main FibHeap routines all operate directly on objects,
    //   push(x), decreaseKey(x), x = popmin()
    // Here x is meant to be a native object for the problem domain, e.g. a Vertex in a graph.
    // For this to work, x has to provide "storage" to allow FibHeap to store the pointers it needs.
    // We can achieve this by requiring Vertex to implement Nodeable.
    //
    // All of the nitty-gritty of the wiring (how we slice an node out of the heap etc.)
    // is described in the accompanying lecture notes section 7.7
    // https://www.cl.cam.ac.uk/teaching/2021/Algorithms/notes2.pdf

    interface Nodeable<T> {
        Node<T> getFibNode();
        double key();
    }
    public static class Node<T> {
        private int degree = 0;
        private boolean isLoser = false;
        private T parent = null;
        private T child = null;
        private T prevSibling = null;
        private T nextSibling = null;
        private double key = Double.POSITIVE_INFINITY;
        private boolean inHeap = false; // to support FibHeap.contains(x)
    }


    T minroot = null;

    void push(T value) {
        value.getFibNode().key = value.key();
        value.getFibNode().inHeap = true;
        if (minroot == null) {
            value.getFibNode().prevSibling = value;
            value.getFibNode().nextSibling = value;
            minroot = value;
        }
        else {
            addSibling(minroot, value, value);
            if (value.key() < minroot.key())
                minroot = value;
        }
    };

    void decreaseKey(T value) {
        T n = value;
        n.getFibNode().key = value.key();
        if (n.getFibNode().parent == null) {
            if (value.getFibNode().key < minroot.getFibNode().key)
                minroot = value;
            return;
            }
        if (n.getFibNode().key >= n.getFibNode().parent.getFibNode().key)
            return;
        while (n.getFibNode().parent != null) {
            makeOrphan(n);
            T u = n.getFibNode().parent;
            n.getFibNode().parent = null;
            n.getFibNode().isLoser = false;
            addSibling(minroot, n, n);
            if (n.getFibNode().key < minroot.getFibNode().key) minroot = n;
            if (u.getFibNode().parent == null) break;
            if (!u.getFibNode().isLoser) { u.getFibNode().isLoser=true; break; }
            n = u;
        }
    }

    T popmin() {
        if (minroot == null)
            throw new IndexOutOfBoundsException("Pop from empty heap");
        T res = minroot;
        T u = minroot.getFibNode().prevSibling;
        T v = minroot.getFibNode().nextSibling;
        // Let u -- minroot -- v
        // If minroot is the only node in the heap, nothing much to do.
        if (u == minroot && minroot.getFibNode().child==null) {
            minroot = null;
            return res;
        }
        // Else, splice out minroot and promote any children
        if (minroot.getFibNode().child != null)
            for (T n : siblings(minroot.getFibNode().child)) {
                n.getFibNode().parent = null;
                n.getFibNode().isLoser = false;
            }
        if (u == minroot) // i.e., if this is the only tree in the rootlist
            minroot = minroot.getFibNode().child;
        else {
            T child = minroot.getFibNode().child;
            u.getFibNode().nextSibling = v;
            v.getFibNode().prevSibling = u;
            if (child != null) addSibling(u, child, child.getFibNode().prevSibling);
            minroot = u;
        }
        // Merge any trees of equal degree
        ArrayList<T> rootArray = new ArrayList<T>(10);
        for (T t : siblings(minroot)) {
            t.getFibNode().prevSibling = t;
            t.getFibNode().nextSibling = t;
            T x = t;
            while (x.getFibNode().degree <= rootArray.size()-1 && rootArray.get(x.getFibNode().degree) != null) {
                u = rootArray.get(x.getFibNode().degree);
                rootArray.set(x.getFibNode().degree, null);
                x = merge(x, u);
            }
            rootArray.ensureCapacity(x.getFibNode().degree + 1);
            while (x.getFibNode().degree > rootArray.size()-1) rootArray.add(null);
            rootArray.set(x.getFibNode().degree, x);
        }
        // Put everything back into a circular rootlist
        T head = null;
        T tail = null;
        for (T t : rootArray) {
            if (t == null) continue;
            if (head == null) {
                head = t;
                tail = t;
            }
            else {
                tail.getFibNode().nextSibling = t;
                t.getFibNode().prevSibling = tail;
                tail = t;
            }
            if (t.getFibNode().key <= minroot.getFibNode().key)
                minroot = t;
        }
        head.getFibNode().prevSibling = tail;
        tail.getFibNode().nextSibling = head;
        // All done!
        res.getFibNode().inHeap = false;
        return res;
    }

    boolean isEmpty() { return (minroot == null); }

    boolean contains(T x) { return x.getFibNode().inHeap; }

    //------------------------------------------------------
    // Assorted utility methods

    private void addSibling(T item, T sib1, T sib2) {
        T next = item.getFibNode().nextSibling;
        item.getFibNode().nextSibling = sib1;
        sib1.getFibNode().prevSibling = item;
        sib2.getFibNode().nextSibling = next;
        next.getFibNode().prevSibling = sib2;
    }

    private void makeOrphan(T item) {
        Node<T> n = item.getFibNode();
        Node<T> p = n.parent.getFibNode();
        T u = n.prevSibling;
        T v = n.nextSibling;
        if (u == item) {
            p.child = null;
            p.degree = 0;
        }
        else {
            if (p.child == item) p.child = v;
            p.degree--;
            u.getFibNode().nextSibling = v;
            v.getFibNode().prevSibling = u;
        }
    }

    private T merge(T x, T y) {
        boolean isXRoot = x.getFibNode().key <= y.getFibNode().key;
        T parent = isXRoot ? x : y;
        T child =  isXRoot ? y : x;
        parent.getFibNode().degree++;
        child.getFibNode().parent = parent;
        if (parent.getFibNode().child == null)
            parent.getFibNode().child = child;
        else
            addSibling(parent.getFibNode().child, child, child);
        return parent;
    }

    // Iterating over the siblings of a node
    private Siblings siblings(T item) { return new Siblings(item); }
    private class Siblings implements Iterable<T> {
        private T start;
        Siblings(T item) { start=item; }
        public Iterator<T> iterator() { return new SiblingsIterator(); }
        private class SiblingsIterator implements Iterator<T> {
            private T cursor;
            private T nextItem = null;
            private boolean done = false;
            SiblingsIterator() { cursor = Siblings.this.start; }
            public boolean hasNext() { return !done; }
            public T next() {
                if (nextItem == null) nextItem = cursor.getFibNode().nextSibling;
                T res = cursor;
                cursor = nextItem;
                nextItem = nextItem.getFibNode().nextSibling;
                done = (cursor == Siblings.this.start);
                return res;
            }
        }
    }

    //------------------------------------------------------
    // OPTIONAL: pretty printing, to produce output that looks like lecture notes

    @Override
    public String toString() {
        if (minroot == null)
            return "<empty heap>";
        ArrayList<String> res = new ArrayList<String>();
        res.add(".");
        for (T t : siblings(minroot)) {
            String childStr = nodeToString(t);
            String[] lines = childStr.split("\\r?\\n");
            for (String line : lines)
                res.add("|" + line);
        }
        res.add("'");
        return String.join("\n", res);
    }

    private String nodeToString(T item) {
        Node<T> n = item.getFibNode();
        String selfStr = item.toString() + "(" + item.getFibNode().key + ")";
        if (n.isLoser) selfStr = "{" + selfStr + "}";
        if (n.child == null) return selfStr;
        ArrayList<String> res = new ArrayList<String>();
        ArrayList<String[]> children = new ArrayList<String[]>();
        for (T child : siblings(n.child)) {
            String childStr = nodeToString(child);
            String[] lines = childStr.split("\\r?\\n");
            children.add(lines);
        }
        int imax = children.size() - 1;
        for (int i=0; i<children.size(); i++) {
            for (int j=0; j<children.get(i).length; j++) {
                char pipe = 'X';
                if (j>0 && i==imax) pipe = ' ';
                else if (j>0) pipe = '|';
                else if (imax==0) pipe = '-';
                else if (i<imax) pipe = '+';
                else pipe = '\\';
                String line = selfStr + pipe + children.get(i)[j];
                res.add(line);
                if (i==0 && j==0) selfStr = selfStr.replaceAll(".", " ");
            }
        }
        return String.join("\n", res);
    }
}