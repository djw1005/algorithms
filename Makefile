CONVERT = jupyter nbconvert --to html
SRCS = graphs.ipynb subgraphs.ipynb advdata.ipynb ucamcl_alg_utils.py fibheap.py

%.html: %.ipynb
	$(CONVERT) $*

all: $(SRCS:.ipynb=.html)

