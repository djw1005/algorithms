# Algorithms

This code accompanies the
Algorithms lecture course for computer science at Cambridge University, taught by [Damon Wischik](https://www.cl.cam.ac.uk/~djw1005/).

* [Printed lecture notes](https://www.cl.cam.ac.uk/~djw1005/redirect_algorithms.html)
* [Videos on YouTube](https://www.youtube.com/playlist?list=PLknxdt7zG11PZjBJzNDpO-whv9x3c6-1H)

